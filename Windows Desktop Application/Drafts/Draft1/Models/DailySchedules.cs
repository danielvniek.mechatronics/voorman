﻿using System;
using System.Windows;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Draft1
{
    public class DailySchedules : ObservableCollection<DailySchedule>
    {
        public DailySchedules() : base()
        {

        }
        public static void Initialise()
        {
            Application.Current.Resources["SearchDates"] = new SearchDates();
            GetSchedule();
        }
        public static void New(Activity Act)
        {
            SQLMethods.New("Schedule", Act);
            GetSchedule();
        }
        public static void Update(Activity Act)
        {
            SQLMethods.Update("Schedule", Act,"ID");
            GetSchedule();
        }
        public static void Delete(Activity Act)
        {
            SQLMethods.Delete("Schedule", Act, "ID");
            GetSchedule();
        }

        public static void GetSchedule()
        {
            //Workers.GetSchedule(startD, endD);
            var SearchDates = Application.Current.FindResource("SearchDates") as SearchDates;
            var startD = SearchDates.startD;
            var endD = SearchDates.endD;
            long startI = Helpers.DateTimeToUnix(SearchDates.startD);
            long endI = Helpers.DateTimeToUnix(SearchDates.endD)+3600*24;
            
            string sql = "select * FROM Schedule where PlannedStart > " + startI + " and PlannedStart < " + endI;
            Trace.WriteLine(sql);
            Activities S = SQLMethods.ReadActs(sql);
            if (S.Count > 0)
            {
                Trace.WriteLine("not empty: count:"+S.Count);
            }
            else
            {
                Trace.WriteLine("empty");
            }
            UpdateDailySchedules(S, startD, endD);
            WorkerSchedules.UpdateWorkerSchedule(S, startD, endD);
        }
        public static void UpdateDailySchedules(Activities Schedule,DateTime startD, DateTime endD)
        {
            List<string> Dates = Helpers.CreateDateListBetweenDates(startD, endD);
            Dictionary<string, Activities> DailyScheds = new Dictionary<string, Activities>();
            foreach (string d in Dates)
            {
                DailyScheds.Add(d, new Activities());
            }
            
            foreach (Activity A in Schedule)
            {
                DateTime StartDate = Helpers.UnixToDateTime(A.PlannedStart);
                Activities Acts = DailyScheds[StartDate.ToString("D")];
                Acts.Add(A);
                DailyScheds[StartDate.ToString("D")] = Acts;
            }
            DailySchedules DS = new DailySchedules();
            foreach (KeyValuePair<string,Activities> KP in DailyScheds)
            {
                DS.Add(new DailySchedule() { Schedule = KP.Value, Date = KP.Key});
            }
            Application.Current.Resources["DailySchedules"] = DS;
        }
    }

    public class SearchDates
    {
        public DateTime startD { get; set; } = DateTime.Today;
        public DateTime endD { get; set; } = DateTime.Today;
    }

    public class DailySchedule
    {
        public string Date { get; set; }

        public Activities Schedule { get; set; }
    }
}
