﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Text;
using System.Dynamic;

using System.Collections.ObjectModel;
using Newtonsoft.Json;
using System.Runtime.CompilerServices;
using System.ComponentModel;


namespace Draft1
{
    public class Activities : ObservableCollection<Activity>
    {
        public Activities() : base()
        {

        }
        public static void Initialise()
        {
            DailySchedules.Initialise();
        }     
       
    }
    //Continue:
    //1) Add description in Act Window & DailySchedule
    //2) check if ID unique
    //3) allow ID generation - farmer can choose 
    //4) click to view and edit act
    //5) then move on to mqtt, history and location
    public class Activity:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        //Properties
        public int ID { get; set; }
        public string Type { get; set; }

        public string Description { get; set; }
        public long PlannedStart { get; set; } = Helpers.DateTimeToUnix(DateTime.Now);
        public DateTime plannedStartDT
        {
            get
            {
                return Helpers.UnixToDateTime(PlannedStart);             
            }
            set
            {
                PlannedStart = Helpers.DateTimeToUnix(value);
            }
        }
        public string plannedStartTime
        {
            get
            {
                return Helpers.UnixToDateTime(PlannedStart).ToString("t");
            }
            set
            {
                DateTime Date = Helpers.UnixToDateTime(PlannedStart).Date;
                string[] hourAndMinutes = value.Split(":");
                int Hour = Convert.ToInt32(hourAndMinutes[0]);
                int Minutes = Convert.ToInt32(hourAndMinutes[1]);
                PlannedStart = Helpers.DateTimeToUnix(Date) + Hour*3600+Minutes*60;
            }
        }
        public DateTime plannedStartDate
        {
            get
            {
                return Helpers.UnixToDateTime(PlannedStart).Date;
            }
            set
            {
                TimeSpan TS = Helpers.UnixToDateTime(PlannedStart).TimeOfDay;
                int totalSecondsSinceMidnight = Convert.ToInt32(TS.TotalSeconds);
                DateTime Date = value.Date;
                PlannedStart = Helpers.DateTimeToUnix(Date) + totalSecondsSinceMidnight;
            }
        }
        public int PlannedDuration { get; set; } = 60;//Duration in minutes
        public string plannedFinishDT { get; set; } = "TODO";//TODO
        public string PlannedBreaks { get; set; } = "{}";//JSON: {DateTimeString:DurationInMinutes,...}
        public long ActualStart { get; set; } = 0;
        public DateTime actualStartDate
        {
            get
            {
                return Helpers.UnixToDateTime(ActualStart).Date;
            }
            set
            {
                TimeSpan TS = Helpers.UnixToDateTime(ActualStart).TimeOfDay;
                int totalSecondsSinceMidnight = Convert.ToInt32(TS.TotalSeconds);
                DateTime Date = value.Date;
                ActualStart = Helpers.DateTimeToUnix(Date) + totalSecondsSinceMidnight;
            }
        }
        public string actualStartTime
        {
            get
            {
                return Helpers.UnixToDateTime(ActualStart).ToString("t");
            }
            set
            {
                DateTime Date = Helpers.UnixToDateTime(ActualStart).Date;
                string[] hourAndMinutes = value.Split(":");
                int Hour = Convert.ToInt32(hourAndMinutes[0]);
                int Minutes = Convert.ToInt32(hourAndMinutes[1]);
                ActualStart = Helpers.DateTimeToUnix(Date) + Hour * 3600 + Minutes * 60;
            }
        }
        public int ActualDuration { get; set; } = 0;//Duration in minutes
        public string stage { 
            get
            {
                if ((ActualStart == 0)&&(ActualDuration==0))
                {
                    return "scheduled";
                }else if (ActualDuration == 0)
                {
                    return "executing";
                }
                else
                {
                    return "completed";
                }
            }
            set { }
        }
    
        public string actualFinishDT { get; set; } = "TODO";//TODO
        public string ActualBreaks { get; set; } = "{}";//JSON: {UnixTime:DurationInMinutes,...}

        public string PlannedWorkers { get; set; } = "{}";//JSON: {WorkerName:{StartTime:UnixStartT,Duration:Minutes,Breaks:{UnixTime:DurationInMinutes,...}},...}
        public string ActualWorkers { get; set; } = "{}";//JSON: {WorkerName:{StartTime:UnixStartT,Duration:Minutes,Breaks:{UnixTime:DurationInMinutes,...}},...}

        public void AddWorker(string WorkerName, string plannedOractual)
        {
            Dictionary<string, string> Ws = new Dictionary<string, string>();
            if (plannedOractual == "planned")
            {
                Ws = Helpers.StringToDictSS(PlannedWorkers);
            }
            else
            {
                Ws = Helpers.StringToDictSS(ActualWorkers);
            }
            string WorkerDets = "{\"Start\":" + PlannedStart + ",\"Duration\":" + PlannedDuration + ",\"Breaks\":\"" + ActualBreaks + "\"}";
            Ws.Add(WorkerName, WorkerDets);
            if (plannedOractual == "planned")
            {
                PlannedWorkers = JsonConvert.SerializeObject(Ws);
                NotifyPropertyChanged("PlannedWorkers");
            }
            else
            {
                ActualWorkers = JsonConvert.SerializeObject(Ws);
                NotifyPropertyChanged("ActualWorkers");
            }
            
        }
        public string MessageToWorkers { get; set; } = "";
        public string FeedbackFromWorkers { get; set; } = "";
        public string Location { get; set; } = "Loc234";//latitude,longitude
        public int LocationLoggingInterval { get; set; } = 5;//in minutes
        public string Open { get; set; } = "False";//can only be true/false

        //Available workers and locations for comboboxes in ActWindows.xaml
        public List<string> availableWorkers
        {
            get
            {
                List<string> availableWs= new List<string>();
                Workers Ws = Application.Current.Resources["Workers"] as Workers;
                foreach (Worker W in Ws)
                {
                    availableWs.Add(W.Name);
                }
                return availableWs;
            }
            set
            {

            }
        }
        public List<string> availableLocations
        {
            get
            {
                return new List<string>();
            }
            set
            {

            }
        }

        public static int GetTotalBreakDuration(string Breaks)
        {
            Dictionary<long, int> BreaksDict = Helpers.StringToDictLI(Breaks);
            int Dur = 0;
            foreach (KeyValuePair<long, int> entry in BreaksDict)
            {
                Dur += entry.Value;
            }
            return Dur;
        }
        public static string GetWorkerInfo(string PlannedOrActualWorkers, string WorkerName, string Info)
        {
            Dictionary<string, string> Workers = Helpers.StringToDictSS(PlannedOrActualWorkers);
            Dictionary<string, string> AllInfo = Helpers.StringToDictSS(Workers[WorkerName]);
            return AllInfo[Info];
        }
        

    }

}
