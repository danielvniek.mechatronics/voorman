﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Diagnostics;

namespace Draft1
{
    public class Workers : ObservableCollection<Worker>
    {
        public Workers() : base()
        {
           
        }
        public static void Initialise()
        {
            GetWorkers();
        }
        public static void New(string Name, DateTime BirthDate, String Rank, DateTime FirstDate, string PhoneNumber, string AppStatus, string ProfilePic)
        {
            var W = Application.Current.FindResource("Workers") as Workers;
            var Worker = new Worker() { BirthDate = BirthDate, Name = Name, Rank = Rank, FirstDate = FirstDate, PhoneNumber = PhoneNumber, AppStatus = AppStatus, ProfilePic = ProfilePic };
            W.Add(Worker);
            SQLMethods.New("Workers", Worker);
        }
        public static void Update(Worker W)
        {
            SQLMethods.Update("Workers", W, "Name");
            GetWorkers();
        }
        public static void Delete(Worker W)
        {
            SQLMethods.Delete("Workers", W, "Name");
            GetWorkers();
        }
        public static void GetWorkers()
        {
            var W = Application.Current.FindResource("Workers") as Workers;
            W = SQLMethods.ReadWorkers("select * from Workers");
            Application.Current.Resources["Workers"] = W;
        }


    }
    public class Worker
    {
        //Properties
        public string Name { get; set; } 
        public DateTime BirthDate { get; set; }
        public string Rank { get; set; }
        public DateTime FirstDate { get; set; }
        public string PhoneNumber { get; set; }
        public string AppStatus { get; set; }
        public string ProfilePic { get; set; }
        /*public Activities schedule
        {
            get;
            set;
        } = new Activities();*/
        
        //Derived properties
        public int age {//USE SMALL LETTER TO INDICATE DERIVED PROPERTIES; SQLMETHODS.NEW DOES NOT SAVE DERIVED PROPERTIES
            get
            {
                DateTime now = DateTime.Now;
                var age = DateTime.Today.Year - BirthDate.Year;

                if (BirthDate.Date > DateTime.Today.AddYears(-age)) age--;
                return age;
            }
        }
        public string experience
        {
            get
            {
                DateTime now = DateTime.Now;
                var years = DateTime.Today.Year - FirstDate.Year;

                var months = DateTime.Today.Month - FirstDate.Month;
                if (months < 0)
                {
                    years--;
                    months = 12 + months;
                }

                if (FirstDate.Date > DateTime.Today.AddMonths(months)) months--;

                string Exp = years + " years, " + months + " months";
                return Exp;
            }
        }

    }

}
