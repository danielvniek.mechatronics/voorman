﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace Draft1
{
    public class WorkerSchedules : ObservableCollection<WorkerSchedule>
    {
        public WorkerSchedules() : base()
        {

        }
        
        public static void UpdateWorkerSchedule(Activities Schedule,DateTime startD, DateTime endD)
        {
            //CONTINUE
            List<string> Dates = Helpers.CreateDateListBetweenDates(startD, endD);
            Workers Workers = Application.Current.FindResource("Workers") as Workers;
            WorkerSchedules WScheds = new WorkerSchedules();
            
            foreach (string D in Dates)
            {
                Dictionary<string, ObservableCollection<WorkerAct>> WADict = new Dictionary<string, ObservableCollection<WorkerAct>>();
                foreach(Worker W in Workers)
                {
                    ObservableCollection<WorkerAct> WActs = new ObservableCollection<WorkerAct>();
                    WADict[W.Name] = WActs;
                }
                

                foreach (Activity A in Schedule)
                {
                    DateTime StartDate = Helpers.UnixToDateTime(A.PlannedStart);
                    if (StartDate.ToString("D") == D)
                    {
                        Dictionary<string, string> PWDict = Helpers.StringToDictSS(A.PlannedWorkers);
                        foreach (KeyValuePair<string, string> entry in PWDict) 
                        {
                            ObservableCollection < WorkerAct > WActs = WADict[entry.Key];
                            WorkerAct NewWA = WorkerAct.Create(A,entry.Key);
                            WActs.Add(NewWA);
                            WADict[entry.Key] = WActs;
                        }
                    }
                }
                ObservableCollection<WorkerDay> WorkerDays = new ObservableCollection<WorkerDay>();
                foreach (Worker W in Workers)
                {
                    ObservableCollection<WorkerAct> WActs = new ObservableCollection<WorkerAct>();
                    if (WADict.ContainsKey(W.Name))
                    {
                        WActs = WADict[W.Name];
                    }
                    
                    WorkerDays.Add(new WorkerDay() { WorkerName = W.Name, WorkerActs = WActs});
                }
                WScheds.Add(new WorkerSchedule { Date = D, WorkerDays = WorkerDays });
            }
            Trace.WriteLine("WorkerScheds");
            Trace.WriteLine(WScheds);
            Application.Current.Resources["WorkerSchedules"] = WScheds;
        }
    }
    
    public class WorkerSchedule
    {
        public string Date { get; set; }
        public ObservableCollection<WorkerDay> WorkerDays { get; set; }

    }
    
    public class WorkerDay
    {
        public string WorkerName { get; set; }
        public ObservableCollection<WorkerAct> WorkerActs { get; set; }
    }
    public class WorkerAct
    {
        public double Left { get; set; }
        public float Top { get; set; }
        public string Height { get; set; }
        public string Content { get; set; }
        public string Background { get; set; }

        
        public static WorkerAct Create(Activity Act,string WorkerName)
        {
            long PlannedStart = Convert.ToInt64(Activity.GetWorkerInfo(Act.PlannedWorkers,WorkerName, "Start"));
            int PlannedDuration = Convert.ToInt32(Activity.GetWorkerInfo(Act.PlannedWorkers, WorkerName, "Duration"));
            string Breaks = Activity.GetWorkerInfo(Act.PlannedWorkers, WorkerName, "Breaks");
            int BreakDur = Activity.GetTotalBreakDuration(Breaks);
            int H = (Helpers.UnixToDateTime(Act.PlannedStart)).Hour;
            int m = (Helpers.UnixToDateTime(Act.PlannedStart)).Minute;
            WorkerAct WA = new WorkerAct();
            WA.Top = (H * 60 + m) / 5;
            WA.Left = 0;//TODO
            int TotalMinutes = PlannedDuration + BreakDur;
            int Height = (TotalMinutes / 5);
            WA.Height = Height.ToString();
            WA.Content = Act.Type + " (" + Act.ID + ")";
            WA.Background = "#FFF";//TODO read from standard acts
            return WA;

            //TODO
        }
        
    }
}
