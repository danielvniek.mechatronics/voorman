﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace Draft1
{
    /// <summary>
    /// Interaction logic for SchedulePage.xaml
    /// </summary>
    public partial class SchedulePage : Page
    {
        public SchedulePage()
        {
            InitializeComponent();
            
           /* System.Diagnostics.Process.Start(new ProcessStartInfo
            {
                FileName = "https://earth.google.com/web/search/-34.030833,22.443086",
                UseShellExecute = true
            });*/
        }

        private void GetSched(object sender, RoutedEventArgs e)
        {
            DailySchedules.GetSchedule();
        }
        private void Create(object sender, RoutedEventArgs e)
        {
            ActWindows AW = new ActWindows();
            AW.SetAct(new Activity(), "Create");
            AW.Show();  
        }
        private void RC_Sched(object sender, RoutedEventArgs e)
        {
            Trace.WriteLine("Clicked");
        }
        private void Edit(object sender, RoutedEventArgs e)
        {
            var item = ((MenuItem)sender).CommandParameter as Activity;
            if (item != null)
            {
                ActWindows AW = new ActWindows();
                AW.SetAct(item,"Update");
                AW.Show();
            }
            Trace.WriteLine("Edit Clicked");
        }
        private void Remove(object sender, RoutedEventArgs e)
        {
            Trace.WriteLine("Remove Clicked");
            Activity item = ((MenuItem)sender).CommandParameter as Activity;
            DailySchedules.Delete(item);
        }
        private void Duplicate(object sender, RoutedEventArgs e)
        {
            var item = ((MenuItem)sender).CommandParameter as Activity;
            if (item != null)
            {
                ActWindows AW = new ActWindows();
                AW.SetAct(item, "Create");
                AW.Show();
            }
            Trace.WriteLine("Duplicate Clicked");
        }
        private void Repeat(object sender, RoutedEventArgs e)
        {
            Trace.WriteLine("Repeat Clicked");
            //TODO
        }
    }
}
