﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Data.SQLite;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Globalization;

namespace Draft1
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class WorkersPage : Page
    {
        public WorkersPage()
        {
            InitializeComponent();
        }
        private void AddWorker(object sender, RoutedEventArgs e)
        {
            string NewName = NewWorkerName.Text;
            DateTime NewDOB = NewWorkerDOB.SelectedDate.Value.Date;
            string Rank = NewWorkerRank.Text;
            DateTime NewFirstDate = NewWorkerFirstDay.SelectedDate.Value.Date;
            string NewNumber = NewWorkerNumber.Text;
            bool NewSmartPhone = NewWorkerSmartPhone.IsChecked.Value;
            string NewAppStatus;
            if (NewSmartPhone == true)
            {
                NewAppStatus = "Waiting for worker app to register";
            }
            else
            {
                NewAppStatus = "No smartphone";
            }
            string ProfilePic="E:/Daniel/Documents/Voorman/Windows Desktop Application/Drafts/Draft1/images/nopp.png";
            Workers.New(NewName, NewDOB, Rank, NewFirstDate, NewNumber, NewAppStatus, ProfilePic);  
        }
        private void Edit_DOB(object sender, RoutedEventArgs e)
        {
            var item = ((MenuItem)sender).CommandParameter as Worker;
            if (item != null)
            {
                InputBox IB = new InputBox("Birth Date:",item.Name,item.BirthDate.ToString("yyyy/MM/dd"));
                string NewBD = IB.ShowDialog();
                try
                {
                    Trace.WriteLine(NewBD);
                    DateTime BD = DateTime.ParseExact(NewBD, "yyyy/MM/dd", new CultureInfo("en-US"));
                    item.BirthDate = BD;
                    Workers.Update(item);
                }
                catch
                {
                    MessageBox.Show("Invalid format. Birth date must be yyyy/MM/dd");
                }

            }
            Trace.WriteLine("Edit Clicked");
            
        }
        private void RC_Workers(object sender, RoutedEventArgs e)
        {

        }
        private void Edit_Rank(object sender, RoutedEventArgs e)
        {
            var item = ((MenuItem)sender).CommandParameter as Worker;
            if (item != null)
            {
                InputBox IB = new InputBox("Rank:", item.Name, item.Rank);
                string NewRank = IB.ShowDialog();
                if((NewRank=="Supervisor")||(NewRank=="Worker"))
                {
                    item.Rank = NewRank;
                    Workers.Update(item);
                }
                else
                {
                    MessageBox.Show("Invalid rank. Rank can only be 'Supervisor' or 'Worker'");
                }
            }
        }
        private void Edit_Number(object sender, RoutedEventArgs e)
        {
            var item = ((MenuItem)sender).CommandParameter as Worker;
            if (item != null)
            {
                InputBox IB = new InputBox("Phone Number:", item.Name, item.PhoneNumber);
                string NewCellNr = IB.ShowDialog();
                item.PhoneNumber = NewCellNr;
                Workers.Update(item);
            }
        }
        private void Edit_Android(object sender, RoutedEventArgs e)
        {
            var item = ((MenuItem)sender).CommandParameter as Worker;
            if (item != null)
            {
                string TrueOrFalse = "True";
                if (item.AppStatus== "No smartphone")
                {
                    TrueOrFalse = "False";
                }
                InputBox IB = new InputBox("Has an android smartphone (True/False)", item.Name, TrueOrFalse);
                string NewAndroid = IB.ShowDialog();
                if (NewAndroid != TrueOrFalse)
                {
                    if (NewAndroid == "True")
                    {
                        NewAndroid = "Waiting for worker app to register";
                    }
                    else
                    {
                        NewAndroid = "No smartphone";
                    }
                    item.AppStatus = NewAndroid;
                    Workers.Update(item);
                } 
            }
        }
        private void Remove(object sender, RoutedEventArgs e)
        {
            Trace.WriteLine("Remove Clicked");
            Worker item = ((MenuItem)sender).CommandParameter as Worker;
            Workers.Delete(item);
        }

    }

}
