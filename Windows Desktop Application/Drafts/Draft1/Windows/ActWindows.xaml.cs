﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;

namespace Draft1
{
    /// <summary>
    /// Interaction logic for ActWindows.xaml
    /// </summary>
    public partial class ActWindows : Window
    {
        private string Action = "Create";
        public ActWindows()
        {
            InitializeComponent();
            this.Title = "Activity ID: Create";
            TimesBox1.ItemsSource = GetTimeList();
            TimesBox2.ItemsSource = GetTimeList();

        }
        public void SetAct(Activity Act,string action)
        {
            this.Resources["Act"] = Act;
            if (action == "Update")
            {
                this.Title = "Activity ID: " + Act.ID;
                Action = action;
            }
            else
            {

                //Get a list of elements in the row/column position
         
                var Children = ActGrid.Children;
                List<UIElement> ChildrenToRemove = new List<UIElement>();
                foreach (UIElement child in Children){
                    if (Grid.GetColumn((FrameworkElement)child) == 2)
                    {
                        ChildrenToRemove.Add((UIElement)child);
                       
                    }
                }
                foreach (UIElement child in ChildrenToRemove)
                {
                    ActGrid.Children.Remove((UIElement)child);
                }
                ActGrid.ColumnDefinitions.RemoveAt(2);
            }
            
            
        }
        public List<string> GetTimeList()
        {
            List<string> T = new List<string>();
            for (int hour = 0; hour < 24; hour++)
            {
                for (int minute = 0; minute < 60; minute++)
                {
                    T.Add(hour.ToString().PadLeft(2,'0') + ":" + minute.ToString().PadLeft(2,'0'));
                }
            }
            return T;
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            if (Action == "Create")
            {
                DailySchedules.New(this.Resources["Act"] as Activity);
            }
            else
            {
                DailySchedules.Update(this.Resources["Act"] as Activity);
            }
            
        }

        private void AddWorker(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            var cbox = new ComboBox();
            if (button.Name == "PW_btn")
            {
                cbox = PW_cbox;
            }
            else
            {
                cbox = AW_cbox;
            }
            
            if (!(cbox.SelectedItem==null))
            {
                Activity Act = this.Resources["Act"] as Activity;
                if (button.Name == "PW_btn")
                {
                    Act.AddWorker(cbox.SelectedItem.ToString(),"planned");
                }
                else
                {
                    Act.AddWorker(cbox.SelectedItem.ToString(),"actual");
                }
                
        
            }
            
        }
    }
}
