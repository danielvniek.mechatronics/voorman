﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.IO;
using System.Diagnostics;
using System.Windows;
using System.Collections.ObjectModel;

namespace Draft1
{
    public static class SQLMethods
    {
        //Foundation: Write and Read
        public static void Write(string sql)
        {
            SQLiteConnection con = new SQLiteConnection("Data Source=FarmData.db");
            con.Open();
            SQLiteCommand command = new SQLiteCommand(sql, con);
            command.ExecuteNonQuery();

        }
        public static Activities ReadActs(string sql)
        {
            Type T = typeof(Activity);
            string QName = T.AssemblyQualifiedName;
            Array ActsA = Read(sql, QName);
            Activities Acts = new Activities();
            for (int i = 0; i < ActsA.Length; i++)
            {
                Acts.Add((Activity)ActsA.GetValue(i));
            }
            return Acts;
        }
        public static Workers ReadWorkers(string sql)
        {
            Type T = typeof(Worker);
            string QName = T.AssemblyQualifiedName;
            Array WorkersA = Read(sql, QName);
            Workers Workers = new Workers();
            for (int i = 0; i < WorkersA.Length; i++)
            {
                Workers.Add((Worker)WorkersA.GetValue(i));
            }
            return Workers;
        }

        public static Array Read(string sql, string QName)
        {
            SQLiteConnection con = new SQLiteConnection("Data Source=FarmData.db");
            con.Open();
            SQLiteCommand command = new SQLiteCommand(sql, con);
            
            Type t = Type.GetType(QName);
            var Instance = Activator.CreateInstance(t);
            var props = Instance.GetType().GetProperties();

            SQLiteDataReader reader = command.ExecuteReader();
            int readerLength = 0;
            while (reader.Read())
            {
                readerLength++;
            }
            reader.Close();
            reader = command.ExecuteReader();
            Array workersA = Array.CreateInstance(t, readerLength);
            int i = 0;
            while (reader.Read())
            {
                var Worker = Activator.CreateInstance(t);
                foreach (var p in props)
                {

                    if (char.IsUpper(p.Name[0]))
                    {
                        if (reader[p.Name].GetType() != typeof(DBNull))
                        {
                            if (p.PropertyType == typeof(DateTime))
                            {
                                p.SetValue(Worker, Convert.ToDateTime(reader[p.Name]));
                            }
                            else if (p.PropertyType == typeof(long))
                            {
                                p.SetValue(Worker, Convert.ToInt64(reader[p.Name]));
                            }
                            else if (p.PropertyType == typeof(int))
                            {
                                p.SetValue(Worker, Convert.ToInt32(reader[p.Name]));
                            }
                            else if (p.PropertyType == typeof(float))
                            {
                                p.SetValue(Worker, (float)reader[p.Name]);
                            }
                            else
                            {
                                p.SetValue(Worker, (string)reader[p.Name]);
                            }
                        }
                    }
                    
                }
                workersA.SetValue(Worker, i);
                i++;
            }

            return workersA;
        }

        public static Array GetAll(string Table, string QName)
        {
            string sql = "select * from " + Table;
            return Read(sql, QName);
            
        }

        public static void New(string Table,object Obj)
        {
            string Columns = "";
            string Values = "";
            var props = Obj.GetType().GetProperties();
            foreach (var p in props)
            { 
                if (char.IsUpper(p.Name[0])&&(p.Name!="ID")){
                    Columns = Columns + p.Name + ",";
                    string Q1 = "'";
                    string Q2 = "'";
                    if ((p.PropertyType == typeof(int)) || (p.PropertyType == typeof(float))){
                        Q1 = "";
                        Q2 = "";
                    }
                    Values = Values + Q1+Obj.GetType().GetProperty(p.Name).GetValue(Obj, null) +Q2+ ",";
                }
               
            }
            Columns = Columns.Remove(Columns.Length - 1, 1);
            Values = Values.Remove(Values.Length - 1, 1);
            string sql = "insert into " + Table + " ("+Columns+") values ("+Values+");";
            Write(sql);
        }
        public static void Update(string Table, object Obj, string Key)
        {
            string UpdateString = "";
            var props = Obj.GetType().GetProperties();
            string K = "'";
            foreach (var p in props)
            {
                if (p.Name == Key) {
                    if ((p.PropertyType == typeof(int)) || (p.PropertyType == typeof(float)))
                    {
                        K = "";
                    }
                }
                if (char.IsUpper(p.Name[0]) && (p.Name != Key))
                {
                    
                    string Q = "'";
                    if ((p.PropertyType == typeof(int)) || (p.PropertyType == typeof(float)))
                    {
                        Q = "";
                    }
                    
                    UpdateString = UpdateString + p.Name + " = "+ Q+ Obj.GetType().GetProperty(p.Name).GetValue(Obj, null) + Q + ",";
                }

            }
            UpdateString = UpdateString.Remove(UpdateString.Length - 1, 1);

            string sql = "update " + Table + " set "+UpdateString+" where "+Key+" = "+ K+Obj.GetType().GetProperty(Key).GetValue(Obj,null)+K+";";
            Trace.WriteLine(sql);
            Write(sql);
        }
        public static void Delete(string Table, object Obj, string Key)
        {
            
            string sql = "delete from " + Table + " where " + Key + " = " + Obj.GetType().GetProperty(Key).GetValue(Obj, null) + ";";
            Write(sql);
        }


    }
}
