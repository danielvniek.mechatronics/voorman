﻿#pragma checksum "..\..\..\..\Windows\ActWindows.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "EFCEE68C01C0717BEC93E1DAE879C345C671905C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Draft1;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;


namespace Draft1 {
    
    
    /// <summary>
    /// ActWindows
    /// </summary>
    public partial class ActWindows : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 25 "..\..\..\..\Windows\ActWindows.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid ActGrid;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\..\Windows\ActWindows.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox TimesBox1;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\..\Windows\ActWindows.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox TimesBox2;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\..\Windows\ActWindows.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox PW_cbox;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\..\Windows\ActWindows.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button PW_btn;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\..\Windows\ActWindows.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox AW_cbox;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\..\Windows\ActWindows.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AW_btn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "5.0.6.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Draft1;component/windows/actwindows.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Windows\ActWindows.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "5.0.6.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ActGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.TimesBox1 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            this.TimesBox2 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 4:
            this.PW_cbox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.PW_btn = ((System.Windows.Controls.Button)(target));
            
            #line 71 "..\..\..\..\Windows\ActWindows.xaml"
            this.PW_btn.Click += new System.Windows.RoutedEventHandler(this.AddWorker);
            
            #line default
            #line hidden
            return;
            case 6:
            this.AW_cbox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.AW_btn = ((System.Windows.Controls.Button)(target));
            
            #line 78 "..\..\..\..\Windows\ActWindows.xaml"
            this.AW_btn.Click += new System.Windows.RoutedEventHandler(this.AddWorker);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 106 "..\..\..\..\Windows\ActWindows.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Save);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

