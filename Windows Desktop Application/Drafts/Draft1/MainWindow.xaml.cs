﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;


namespace Draft1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
            this.WindowState = WindowState.Maximized;
            Workers.Initialise();
            Activities.Initialise();
        }

        //NAVIGATION
        //Data:
        private void NavToSchedule(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new SchedulePage());
        }
        private void NavToHistory(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new HistoryPage());
        }
        //Configuration:
        private void NavToWorkers(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new WorkersPage());
        }
        private void NavToStandardActs(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new StandardActsPage());
        }
        private void NavToSalaryGroups(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new SalaryGroupsPage());
        }
        private void NavToLocations(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new LocationsPage());
        }

    }
}
