# Description #
Voorman is a worker management software that consists of 
- Windows Desktop Application (WPF) using a local SQlite database
- Manager Android App (Kotlin)
- Worker Android App (Kotlin)
- Cloud MQTT Broker

The figure below shows how a farm's devices will communicate with each other\
<img src="/doc/RM1.jpg" height="200">\

The figure below shows how multiple farm's can be managed by a single developer\
<img src="/doc/RM2.jpg" height="200">\

So far only the first draft of the WFP app has been developed. Below are some screenshots from this app.\
<img src="/doc/RM3.JPG" height="500">\
<img src="/doc/RM4.JPG" height="500">\
<img src="/doc/RM5.JPG" height="500">\
